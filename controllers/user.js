const User = require('../models').User;
const { successResponse, errorResponse } = require('../helpers/responseCode');

module.exports = {
  create(req, res) {
    const { name, username, email, password } = req.body
    return User.create({
      name, username, email, password
    })
    .then(data => res.status(201).send(successResponse(data)))
    .catch(err => res.status(400).send(errorResponse(err)))
  },

  listAll(req, res) {
    return User.findAll()
    .then(data => res.status(200).send(successResponse(data)))
    .catch(err => res.status(400).send(errorResponse(err)))
  },

  listShow(req, res) {
    const {user_id} = req.params
    return User.findByPk(user_id)
    .then(data => res.status(200).send(successResponse(data)))
    .catch(err => res.status(400).send(errorResponse(err)))
  },
};