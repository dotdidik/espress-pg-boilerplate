# FROM node:10
FROM node:11-alpine

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install -g sequelize-cli
RUN npm install
# RUN npm install sequelize pg pg-hstore
# RUN npm install -g sequelize-cli
COPY . .

EXPOSE 4000
# CMD ["sequelize", "db:migrate"]
# CMD ["npm", "start"]
# CMD ["node", "app.js"]