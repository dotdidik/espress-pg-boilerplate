module.exports = {
  successResponse(result) {
    return {
      status: true,
      result: result,
      errors: null
    }
  }, 

  errorResponse(err) {
    return {
      status: false,
      result: null,
      errors: err
    }
  }
}