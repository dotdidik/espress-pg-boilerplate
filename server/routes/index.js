const userController = require('../../controllers').user;

module.exports = (app) => {
  app.post('/user', userController.create);
  app.get('/user', userController.listAll);
  app.get('/user/:user_id', userController.listShow);
};